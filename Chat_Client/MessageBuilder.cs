﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Chat_Client
{
    public class MessageBuilder
    {
        //private MessageTypes mType;
        public string mType;
        public string Text;
        public string sender;
        public long datetime;

        public MessageBuilder()
        {
        }

        //public MessageBuilder(MessageTypes mtype , String MessageText, String sender)
        //{
        //    this.mType = mtype;
        //    this.Text = MessageText;
        //    this.sender = sender;
        //}

        public MessageBuilder(string mtype, string MessageText, string sender)
        {
            this.mType = mtype;
            this.Text = MessageText;
            this.sender = sender;
            this.datetime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        public override string ToString()
        {
            return Text+datetime;
        }
    }
}
