﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat_Client
{
    public class MessageTypes
    {
        public static readonly MessageTypes GREETING = new MessageTypes("Hi");
        public static readonly MessageTypes REQUEST_OLD_MESSAGES = new MessageTypes("request_old_messages");
        public static readonly MessageTypes SEND_MESSAGE = new MessageTypes("send_message");

        private string value;
        
        public MessageTypes(String type)
        {
            this.value = type;
        }

        public string toString()
        {
            return value;
        }

        public MessageTypes fromString(string type)
        {
            MessageTypes mtype = null;
            if (type != null)
            {
                switch (type)
                {
                    case "Hi":
                        mtype = GREETING;
                        break;
                    case "request_old_messages":
                        mtype = REQUEST_OLD_MESSAGES;
                        break;
                    case "send_message":
                        mtype = SEND_MESSAGE;
                        break;
                }
            }
            return null;
        }
    }
}
