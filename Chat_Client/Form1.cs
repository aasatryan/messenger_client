﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Chat_Client
{
    public partial class Form1 : Form
    {
        List<Message> history = new List<Message>();
        Queue<Message> messages = new Queue<Message>();
        Object alock = new Object() { };

        static string server = "127.0.0.1";
        static Int32 port = 13000;
        string username = "";
        TcpClient client;
        string message;
        Thread waiting_thread;
        Thread write_thread;
        Thread senderThread;
        NetworkStream stream;
        int counter = 0;
        Byte[] bytes = new Byte[256];
        String received_data = null;
        bool connected = false;

        public Form1()
        {
            InitializeComponent();
        }

        void messageWaiter(object avail)
        {
            //lock(alock)
            //{
            if (!(bool)avail)
            {
                //stream.Read()
                //Monitor.Pulse(alock);
                return;
            }
            else
            {
                stream = client.GetStream();
                int i = 0;
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Translate data bytes to a ASCII string.
                    received_data = Encoding.ASCII.GetString(bytes, 0, i);
                    ((TextBox)(Controls["textbox1"])).AppendText(received_data + Environment.NewLine);
                    //history.Add(new Message());
                    //Console.WriteLine("Received: {0}", received_data);
                    //MessageBox.Show(received_data, "New message received!");
                    // Process the data sent by the client.
                    //received_data = received_data.ToUpper();

                    //byte[] msg = System.Text.Encoding.ASCII.GetBytes(received_data);

                    // Send back a response.
                    //stream.Write(msg, 0, msg.Length);
                    //Console.WriteLine("Sent: {0}", received_data);

                }

                //Monitor.Pulse(alock);
                //Monitor.Wait(alock);
                //}
            }

        }

        private void sendMessage(MessageBuilder builder)
        {
            string JSonString = new JavaScriptSerializer().Serialize(builder);
            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(JSonString);
            try
            {
                stream = client.GetStream();
                stream.Flush();
                stream.Write(data, 0, data.Length);
                stream.Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error occured");
            }
        }

        /// <summary>
        /// To change server configuration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            server = Controls["textbox_server_ip"].Text;
            port = Int32.Parse(Controls["textbox_server_port"].Text);
        }

        /// <summary>
        /// chat text message send button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Send_Click(object sender, EventArgs e)
        {
            message = ((TextBox)this.Controls["TextBox2"]).Text;
            sendMessage(new MessageBuilder(MessageTypes.SEND_MESSAGE.toString(), message, username));

        }
        /// <summary>
        /// request old messages button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_request_old_messages_Click(object sender, EventArgs e)
        {
            MessageBuilder builder = new MessageBuilder(MessageTypes.REQUEST_OLD_MESSAGES.toString(), "", username);
            sendMessage(builder);
        }
        /// <summary>
        /// connect to server clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Button loginButton = (Button)(Controls["button1"]);
            if (((TextBox)Controls["textBox_login"]).Enabled)
            {
                try
                {
                    client = new TcpClient(server, port);
                    MessageBuilder greetingBuilder = new MessageBuilder(MessageTypes.GREETING.toString(), "", username);
                    sendMessage(greetingBuilder);
                    username = ((TextBox)this.Controls["textBox_login"]).Text;
                    //waiting_thread = new Thread(messageWaiter);
                    //waiting_thread.Start(true);

                    loginButton.Text = "log out";
                    ((TextBox)(Controls["textBox_login"])).Enabled = false;

                }
                catch (Exception exc)
                {
                    MessageBox.Show("Server is not active", "Info");
                }
            }
            else
            {
                client.Close();
                username = "";
                ((TextBox)Controls["textBox_login"]).Enabled = true;
                loginButton.Text = "log in";
            }
        }
    }
}
